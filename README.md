# Project specifications
[tracking sheet](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/12/docs/-/blob/master/suivi_de_projet.md)

## Objectives
The main purpose of our project is to enhance Polytech's virtual workplace by implementing new features. Distrubuted Application such as (Zoom, Discord, Teams, Slack ... will the main subject of our project. </br>


## Functionnality
As mentionned before, we will come up with several ideas which could enhance virtual workplace. These are the platforms that we've chosen to work on :
* [BigBlueButton/Zoom](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/12/docs/-/blob/master/pbm_installation.md)
* [Discord](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/12/docs/-/blob/master/discord.md)
