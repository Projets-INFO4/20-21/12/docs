# Inventory of problems encountered during installs

## BBB
For each BBB installation option, a functional server with specific features is required: 

- Ubuntu 16.04 64-bit
- 16 Gb of RAM
- a CPU of 8 cores
- 500 Gb of available disk space
- a valid domain name to have an SSL certificate
- IPv4 and IPv6 addresses

Unfortunately, it was impossible for us to have a server with such features.
To solve these problems we have tried to perform the installations with Docker or with a virtual machine. Only, in both cases, you need a domain name with a valid SSL certificate.

### Docker
The only version we have successfully installed is a demo version of the client, so it cannot work without a server.

### Virtual Machine
When using a VM with an image from Ubuntu 16.04, the command to configure the : 
```shell=
wget -qO- https://ubuntu.bigbluebutton.org/bbb-install.sh | bash -s -- -w -a -v xenial-22
```
returns: 
![](https://i.imgur.com/ZvQQNZs.png)

We tried to install the packages manually. However, some packages such as bbb-apps-video-broadcast failed. Moreover, none of our machines contain 8 cores. 


## Zoom
In order to find another solution for our project, we decided to try the same thing but with Zoom. Only we only managed to get the demo version on the client side which is not enough to develop the features we wanted. We made a request for the full version but we didn't get an answer.


Translated with www.DeepL.com/Translator (free version)
