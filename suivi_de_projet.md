# Tracking Sheet

# Members
Guillaume VACHERIAS,
Jules HERY,
Amad SALMON

## 05/04/2021
* Project presentation and report finished

## 04/04/2021
* Pursuing report (work left : section demo) 
* Completed help command
* Added automatic pin message with document


## 01/04/2021
* Begin report
* implementing slash command : it is possible to implement slash command but we highly doubt that we will have enough time
* merge with jules.dev with master : small modifications, added install.sh and run.sh for the working environment

## 23/03/2021
* implementing slash command : it works but there's still problems with promises
* event reminder 
* asynchrone function for calendar feature

## 22/03/2021
* merge guillaume.dev with master (notifyAll, notify, random features implemented)
* merge jules.dev with master (reminder, homework features implemented)
* event reminder 

## 15/03/2021
* Random student selection implemented
* Pursuing homeword reminder implementation (almost done)
* Pursuing database and bot connexion
* Implementing bot calendar feature

## 09/03/2021
* Begin random student selection implementation 
* Update help command
* Refactoring index.js
* Fixing bugs

## 08/03/2021
* Pursuing bot implementation
* Notify voice channel implemented

## 02/03/2021
* Pursue of the bot's implementation 
* Implement link between database and the bot
* Implement link between calendar and the bot

## 01/03/2021
* Begin bot implementation
* Begin linking the database and the bot
* Downloading and parsing of the schedule

## 23/02/2021
* Inventory of every failure during the installation finished
* Proposal of functionalities for the bot
* Discovery of the Discord Bot API

## 22/02/2021
* Installation of Greenlight which needs a working BBB server
* Inventory of every failure during the installation
* Proposal of switching to the implementation of a BOT on discord and Mattermost as well

## 08/02/2021
* Docker and VM installation were also not successful
* We've chose to work with ZOOM. (waiting for Nicolas Palix response)

## 01/02/2021
* Manual install unsuccessful. Alternative : docker and VM

## 25/01/2021
* Survey on BBB platform : Potential enhancement and pros, cons of this platform
* Begin tools installation for BBB (Manual install)


## 18/01/2021
* Project selection : ENT Polytech
