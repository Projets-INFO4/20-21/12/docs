# Discord enhancement
We propose new features for Discord by using a bot : 
* Homeworks and projects reminder
* Sending online class links (zoom, bbb...) several minutes before class
* Direct message to people in a specific voice channel
* Automatic ping of message containing important document
